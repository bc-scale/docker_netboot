# choosing base image
FROM alpine:3.15

# installing required packages
RUN apk add --no-cache dnsmasq syslinux apache2 p7zip

# copying files to container
COPY root/ /

# download iPXE
RUN mkdir -p /var/lib/tftpboot; wget -P /var/lib/tftpboot https://boot.ipxe.org/ipxe.efi

# declaring volume for data
VOLUME ["/storage"]

# choosing system starting point
ENTRYPOINT ["/init.sh"]
