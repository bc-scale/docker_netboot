keyfile := ~/.ssh/id_ed25519_lab

help:
	@echo -e "  help       this message"
	@echo -e "  key_gen    generate ed25519 ssh key"
	@echo -e "  confprep   set ssh key and password in cloud-init file"
	@echo -e "  confcheck  check if cloud-init file has any errors"
	@echo -e "  build      build docker_netboot container"
	@echo -e "  up         run the container in the background"
	@echo -e "  down       stop the container and clean up environment"

key_gen:
	ssh-keygen -t ed25519 -f ${keyfile}

confprep:
	awk -i inplace -v INPLACE_SUFFIX=.bak '{ gsub(/<SSH KEY>/,"$(shell cat ${keyfile}.pub)") }; { print }' ./storage/user-data
	awk -i inplace '{ gsub(/<PASSWORD>/,"$(shell openssl passwd -6)") }; { print }' ./storage/user-data

confcheck:
	cloud-init devel schema -c ./storage/user-data

build:
	docker-compose build

up:
	docker-compose up -d --remove-orphans

down:
	docker-compose down --remove-orphans
