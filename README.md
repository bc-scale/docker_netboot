# Docker Netboot v2

It's an image created for automatic installation of linux distributions which support [cloud-init](https://cloud-init.io/), such as Ubuntu. It's intended to be used in small networks which don't require more advanced solutions.
You can also use it for ipxe booting without automation.

---

## Prerequisites

Allow traffic for dhcp, http and tftp on your firewall. If you are using firewalld then you can run these commands to enable them:

```sh
sudo firewall-cmd --add-service=dhcp --permament
sudo firewall-cmd --add-service=http --permanent
sudo firewall-cmd --add-service=tftp --permanent
sudo firewall-cmd --reload
```

Required packages:
	- docker.io, docker-compose
	- openssl (for creating password hash)
	- cloud-init (for checking user-data file)

---

## Usage

Edit:
	- (required) setup.env: enter devices ip address which is in the network you want to use.
	- storage/user-data: you can do it manually or use `make confprep`

You can do all the stuff manually or use `make`.

```sh
# output of make help:

  help       this message
  key_gen    generate ed25519 ssh key
  confprep   set ssh key and password in cloud-init file
  confcheck  check if cloud-init file has any errors
  build      build docker_netboot container
  up         run the container in the background
  down       stop the container and clean up environment
```

---

